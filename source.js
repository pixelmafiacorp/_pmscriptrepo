console.debug('PM Website Confirmation: Script successfully loaded');

let getImageLabel = () => {
  $('.image-label').each(function (index,el) {
    let dataID = $(this).attr('data-image-label');		
    $('.image-label > .label').eq(index).html(dataID);
  });
}

let automateURL_forPageFlow = () => {
  let currentURL 	= window.location.href,
      regex 		= new RegExp('/[^/]*$'),
      newURL 		= currentURL.replace(regex, '/'),
      elem      = $('.page-flow-section-block > .page-flow--section > .page-flow--element');	

  elem.each(function () {
    let slug = $('.page-flow-slug', this).html();
    $(this).attr('data-slug', slug);
    $('.page-flow-slug', this).remove();
  });

  elem.on('click', function (e) {
    let slug = $(this).data('slug'),
        URL = newURL+''+slug;

    if(e.ctrlKey || e.metaKey) { window.open(URL, '_blank'); }
    else { window.location.href = URL; }
    
  });
}

let automateURL = (button, slug) => {
  let currentBtn = button,
      currentURL = window.origin;

  $(`${currentBtn}`).each(function () {
    let currentSlug = $(this).prev(`${slug}`).html();
    $(this).attr('data-slug', currentSlug);
    $(this).prev(`${slug}`).remove();
  }),

    $(`${currentBtn}`).on('click', function (e) {
      let slug 				= $(this).data('slug'),
          projectsURL 		= currentURL+''+'/projects/'+slug,
          servicesURL 		= currentURL+''+'/about-us/services/'+slug,
          currentButtonData = $(this).data('button');

      if ( currentButtonData == 'projects' ) {
        if (e.ctrlKey || e.metaKey) { window.open(projectsURL, '_blank'); }
        else { window.location.href = projectsURL; }
        
      } else if ( currentButtonData == 'services' ) {
        if (e.ctrlKey || e.metaKey) { window.open(servicesURL, '_blank'); }
        else {  window.location.href = servicesURL; }
        
      }
  });
}

let automateURL_forSliders = () => {
  let buttonClass = '.btn.link-button',
      slugClass = '.explore-btn-slug';

  automateURL(buttonClass, slugClass);
}

let automateURL_subNavigation = () => { 
  let buttonClass = '.sub-navigation-item',
      slugClass = '.sub-nav-slug';

  automateURL(buttonClass, slugClass);
}

/** This is to make sure that whenever the user deletes
	the uploaded file. The data-value will be totally removed **/
let removeDataValueSetOnFileUploadField = () => {
  $('.w-icon-file-upload-remove').on('click', function() {
    $('input[type="file"]').removeData('value');
  });
}

let hideCareerApplicationForms__onSmallScreens = () => {
  if(window.innerWidth <= 1024) {
    $('#careers-application').remove();
    $('.application-form').remove();
    $('.application-form-modal').remove();
    $('.open-form-btn').remove();
  }
}

Webflow.push(function() {

  // Add Page label decorator automatically based on the title
  $('.page-label').html($(document).attr('title'));

  $('body').append('<div class="header-gradient"></div>');

  // Menu Interaction
  $('.menu-icon').click(function(e) {
    e.preventDefault();

    var menuVisibility = $('.nav-area').css('display');

    (menuVisibility == 'none') ?
      $('body').css('overflow', 'hidden'):
    $('body').css('overflow', 'auto'); 

    $('html').toggleClass('overflow-hidden');
  });

  // ----------------------------------------------------
  // Tab item

  $('.tab-item').on('click', function() {
    var $el = $(this);

    // Contain the selection of tab item to the
    // selection container and not all tab-item
    $el.parentsUntil('.panel-selection')
      .find('.tab-item')
      .removeClass('active');

    $el.addClass('active');
  });

  getImageLabel();
  automateURL_forPageFlow();
  automateURL_forSliders();
  automateURL_subNavigation();
  removeDataValueSetOnFileUploadField();
  hideCareerApplicationForms__onSmallScreens();
});


/*-------------- Scroll Marker --------------*/
let initScrollMarker = () => {
  let pageAnchors = $('[anchor-name]');
  let $scrollMarker = $('.scroll-marker-block');

  let injectScrollMarker = (anchorName, pointer) => {
      $scrollMarker.append(
          `<div class="scroll-marker" data-label="${anchorName}" data-pointer="pointer-${pointer}">
              <span class="label">${anchorName}</span>
          </div>`
      );
  };

  $scrollMarker.append()
  pageAnchors.each((a, el) => {
      let $anchor = $(el);
      let anchorName = $anchor.attr('anchor-name');

      $anchor.prepend(`<a class="_marker" name="pointer-${a}"></a>`);
      injectScrollMarker(anchorName, a);
  });

  $('.scroll-marker').on('click', (el) => {
      let $el         = $(el.currentTarget);
      let pointerName = $el.attr('data-pointer');
      let position    = $(`[name="${pointerName}"]`).offset().top;

      // Move to the position
      anime({
          targets  : $('html,body')[0],
          scrollTop: position,
          duration : 700,
          easing   : "easeInOutExpo"

      });

      $el.addClass('active');
  });


  let markers       = $('._marker');
  let scrollMarkers = $('.scroll-marker');
  let $window       = $(window);

  const TOP_TRESHOLD = -400;

  let highlightActivePosition = () => {
      // check the position of the markers relative to the scroll
      let scrollPosition = $window.scrollTop();
      let windowHeight   = $window.height();

      markers.each((a, el) => {
          let $el                   = $(el);
          let $container            = $el.parent();
          let containerHeight       = $container.height();
          let distanceFromTheTop    = scrollPosition - $el.offset().top;
          let distanceFromTheBottom = (distanceFromTheTop + containerHeight) - windowHeight;

          if (distanceFromTheTop >= TOP_TRESHOLD && distanceFromTheBottom <= windowHeight / 2) {
              scrollMarkers.removeClass('active');
              $(`[data-pointer="${$el.prop('name')}"]`).addClass('active');
          }
      });
  };

  $(document)[0].addEventListener("scroll", highlightActivePosition);

  highlightActivePosition();
}

initScrollMarker();

  
/*-------------- Best Browser Option --------------*/
let get_browser_version = () => {
  let ua = navigator.userAgent,tem,M=ua.match(/(opera|chrome|crios|edg|edgios|safari|firefox|fxios|msie|trident(?=\/))\/?\s*(\d+)/i) || []; 
  if( /trident/i.test(M[1]) ){
    tem=/\brv[ :]+(\d+)/g.exec(ua) || []; 
    return { name:'IE', version:(tem[1]||'')};
  }   
  if( (M[1] === 'Chrome') || (M[1] === 'CriOS') || (M[1] === 'FxiOS') || (M[1] === 'EdgiOS')){
    tem=ua.match(/\bOPR|Edg\/(\d+)/)
    if(tem!=null) { return { name:'Edge', version:tem[1]}; }
  }

  M=M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
  if((tem=ua.match(/version\/(\d+)/i))!=null) { M.splice(1,1,tem[1]); }
  return { name: M[0], version: M[1] };
}

let validateBrowserVersion = () => {
  let browser = get_browser_version(),
      smallDevice = window.innerWidth <= 1024;

  if (!smallDevice) { 
    if (
      ((browser.name == 'Chrome') && (browser.version < 83)) || 
      ((browser.name == 'Firefox') && (browser.version < 80)) ||
      ((browser.name == 'Safari') && (browser.version < 13)) ||
      ((browser.name == 'Edge') && (browser.version < 83))
    ) { 
      $('.current-version').html(`${browser.name} v.${browser.version}.`);
      $('.browser-option').removeClass('hidden');
    } else {
      $('.browser-option').remove();
    }

  } else {
    if (
      ((browser.name == 'Chrome') && (browser.version < 83)) ||
      ((browser.name == 'Edge') && (browser.version < 83)) ||
      ((browser.name == 'Firefox') && (browser.version < 79)) ||
      ((browser.name == 'Safari') && (browser.version < 9)) ||
      ((browser.name == 'CriOS') && (browser.version < 83)) ||
      ((browser.name == 'FxiOS') && (browser.version < 25))
    ) { 

      /** iOS check for Chrome and Firefox **/
      if (browser.name == 'CriOS') {
        browser.name = 'Chrome';
      } else if (browser.name == 'FxiOS') {
        browser.name = 'Firefox';
      } else if ((browser.name == 'EdgiOS')) {
        browser.name = 'Edge';
      }

      $('.current-version').html(`${browser.name} v.${browser.version}.`);
      $('.browser-option').removeClass('hidden');

    } else {   
      $('.browser-option').remove();
    }
  }
} 

/**validateBrowserVersion();**/

let closePopUpMessage = () => {
  $('.close-btn.reco-browser').on('click', function () {
    /**$('.browser-option').hide();**/
    $('.landscape-pop-up').hide();
  });
}

closePopUpMessage();

/*-------------- Detecting Mobile OS --------------*/

let detectMobileOS = () => {
  let userOS,
      userOSver,
      ua = navigator.userAgent,
      uaindex,
      isSmallDevice = window.innerWidth <= 1024;

  if ( ua.match(/iPad/i) || ua.match(/iPhone/i) ){
    userOS = 'iOS';
    uaindex = ua.indexOf( 'OS ' );
  } else if ( ua.match(/Android/i) ) {
    userOS = 'Android';
    uaindex = ua.indexOf( 'Android ' );
  } else {
    userOS = 'unknown';
  }

  if ( userOS === 'iOS'  &&  uaindex > -1 ) {
    userOSver = ua.substr( uaindex + 3, 3 ).replace( '_', '.' );
  } else if ( userOS === 'Android'  &&  uaindex > -1 ){
    userOSver = ua.substr( uaindex + 8, 3 );
  } else {
    userOSver = 'unknown';
  }

  if (isSmallDevice) { 
    if ( (userOS === 'iOS') && (userOSver < 10)) { 
      $('.careers-videobg').hide();
    } else if ( (userOS === 'Android') && (userOSver < 5) ) {
      $('.careers-videobg').hide();
    }
  }
  // console.log(userOS + " " + userOSver);
}

detectMobileOS();

// Careers Page
let addPostionCardCount = () => {
  let count = 1,
      availablePosition = $('.available-position-item').length;

  $('.available-position-item').each(function () {
    $(this).find('.card-count').html('0' + count);
    count++;
  });
  $('.position-counter').html(availablePosition);
}

addPostionCardCount();

let charactersOnly = (fieldID) => {
  $(`#${fieldID}`).on('input keypress', function (e) {
    let regex =  /^[A-Za-zÀ-Üà-ü'.-\s\b]+$/i,
        isValid = regex.test(String.fromCharCode(!e.charCode ? e.which : e.charCode));

    if (!isValid) {
      e.preventDefault();
    }
  });
}

let numbersOnly = () => {
  $('input[type="tel"]').on('input keypress', function() {
    var key = event.keyCode || event.charCode || event.which;
    switch (key) {
      case 43:
      case 40:
      case 41:
        break;
      default:
        var regex = new RegExp("^[0-9.,/ $@()]+$");

        if ((key >= 48 && key <= 57) || key == 8 ||
            (key >= 35 && key <= 40) || key == 46) {
        } else {
          event.preventDefault();
          return false;
        }
        break;
    }
  });
}

numbersOnly();

let makeSelectFieldItemsDynamic = () => {
  let selectionItemCreatives 	= $('.selection-item--creatives'),
      selectionItemOperations = $('.selection-item--operations');

  selectionItemCreatives.each(function() {
    let item = $(this).text();
    $('#Position > option:first-of-type').after(`<option value="${item}">${item}</option>`);
  }),

  selectionItemOperations.each(function() {
    let item = $(this).text();
    $('#PositionField > option:first-of-type').after(`<option value="${item}">${item}</option>`);
  });
  
  if ($('.selection-item--creatives').length > 0) {
    $('.selection-items-collection--creatives').remove();
  } else {
    $('.selection-items-collection--creatives').show();
  }
  
  if ($('.selection-item--operations').length > 0) {
    $('.selection-items-collection--operations').remove();
  } else {
    $('.selection-items-collection--operations').show();
  }
  
  /**Disable Selection Field Placeholder**/
  $('.select-field option:nth-child(1)').prop('disabled', true);
}

makeSelectFieldItemsDynamic();

let selectFieldOnChange_valueColor = () => {
  $('.select-field').on('change', function () {
    $(this).addClass('active');
  });
}

selectFieldOnChange_valueColor();

let onFormOpen__OverflowHidden = () => {
  $('.open-form-btn').on('click', function () {
    $('body').addClass('overflow-hidden');
    $('html').addClass('overflow-hidden');
  });
}

onFormOpen__OverflowHidden();

let resetForm = () => {
  $('select').prop('selectedIndex', 0).removeClass('hasError');
  $(".select-field").removeClass('active');
  
  $('input').val('');
  $('textarea').val('');
  $('input[type="submit"]').val('Submit');
  $('input[type="file"]').removeAttr('data-value');
  
  $('#creatives-form').removeAttr('style');
  $('#operations-form').removeAttr('style');
  
  $('.w-form-done').removeAttr('style');
  $('.w-form-fail').removeAttr('style');
  $('.w-file-upload-uploading').removeAttr('style');
  $('.w-file-upload-success').removeAttr('style');
  $('.w-file-upload-error').removeAttr('style');
  $('.upload-resume-field').removeAttr('style');
  $('.w-file-upload-file-name').text('');
  
  $('.tab-menu').removeClass('hide');
  $('.tab-selection-label').removeClass('hide');
  
  $('.application-form-error').removeClass('visible');
  
  setTimeout(function(){ 
    $('body').removeClass('overflow-hidden');
    $('html').removeClass('overflow-hidden');
  }, 500);

  $('#tab-creatives').trigger('tap');

  $('#operations-form').validate().resetForm();
  $('#creatives-form').validate().resetForm();
  $('.career-field').removeClass('hasError');
  $('.career-field > .error').removeClass('error');
  $('.career-field > .error').removeAttr('aria-describedby');
  $('.career-field > .error').removeAttr('aria-invalid');
}

let resetFormOnTabChange = () => {

  $('select').prop('selectedIndex', 0).removeClass('hasError');
  $(".select-field").removeClass('active');
  
  $('.w-form-done').removeAttr('style');
  $('.w-form-fail').removeAttr('style');
  $('.w-file-upload-uploading').removeAttr('style');
  $('.w-file-upload-success').removeAttr('style');
  $('.w-file-upload-error').removeAttr('style');
  $('.upload-resume-field').removeAttr('style');
  $('.w-file-upload-file-name').text('');

  $('.application-form-error').removeClass('visible');
  
  $('input').val('');
  $('textarea').val('');
  $('input[type="file"]').removeAttr('data-value');
  $('input[type="submit"]').val('Submit');
  
  $('#operations-form').validate().resetForm();
  $('#creatives-form').validate().resetForm();
  $('.career-field').removeClass('hasError');
  $('.career-field > .error').removeClass('error');
  $('.career-field > .error').removeAttr('aria-describedby');
  $('.career-field > .error').removeAttr('aria-invalid');
}

let onCareersTabsOnChange = () => {
  $('.w-tab-link').on('click', function () {
    resetFormOnTabChange();
  });
}

onCareersTabsOnChange();

let modalClose__Reset = () => { // ESC
  $( document ).on( 'keydown', function ( e ) {
    if ( e.keyCode === 27 ) {
      document.getElementById('form-close-btn').click();
    }
  });

  $('.close-button').on('click', function () {
    resetForm();
  });
}

modalClose__Reset();

let specifyFormToShow = () => {
  if(window.innerWidth > 1024) {
    document.getElementById("career-entry-position").value = $('#career-entry-title').text();

    if ($('#career-type').html() == 'Creatives') {

    } else if ($('#career-type').html() == 'Operations') {
      var myobj = document.getElementById("portfolio-link-field");
      myobj.remove();
      $('form').attr('id','careers-application-operations');
    }
  }
}

if ($('#careers-application').length) {
  specifyFormToShow();
}

// ----------------------------------------------------
// Form Validate

/** -- Careers Page - Creatives Form --**/

charactersOnly('Name');

let validateCreativesForm = () => {
  $.validator.addMethod("lettersOnly", function(value, element) {
    return this.optional(element) || /^[A-Za-zÀ-Üà-ü'.-\s]+$/i.test(value);
  }, "Please use regular letters only.");
  
  $.validator.addMethod("emailordomain", function(value, element) {
    return this.optional(element) || /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/.test(value) || /[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/.test(value);
  }, "Please enter a valid email address.");

  $.validator.addMethod("mobilePattern", function(value, element) {
      return this.optional(element) || /^[+\d](?:.*\d)?$/.test(value);
      }, "Please use numbers only.");

  $.validator.addMethod("validPortfolio", function(value, element) {
    return this.optional(element) || /(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]+\.[^\s]{2,}|www\.[a-zA-Z0-9]+\.[^\s]{2,})/gi.test(value);
  }, "Invalid portfolio link!");

  $('#creatives-form').validate({
    onsubmit: false,
    errorPlacement: function(error, element) {
      $(element)
        .closest("form")
        .find("label[for='" + element.attr("id") + "']" )
        .closest('.career-field').addClass('hasError')
        .append(error);
    },
    rules: {
      Position: {
          required: true,
      },
      Name: {
        required: true,
        lettersOnly: true,
      },
      EmailAddress: {
        required: true,
        emailordomain: true
      },
      MobileNumber: {
          required: true,
          mobilePattern: true,
          minlength: 7
      },
      PhoneNumber: {
          mobilePattern: true,
          minlength: 7
      },
      PortfolioLink: {
          required: true,
          validPortfolio: true
      },
      Message: {
          required: true,
      },
      ResumeCV: {
        required: true
      }
    },
    errorElement: "span",
    messages: {
      Position: {
         required : "What position are you applying for?"
      },
      Name: {
        required: "Please tell us your name!"
      },
      EmailAddress: {
        required: "Don't forget your email!"
      },
      MobileNumber: {
        required: "Please share your complete number.",
        minlength: "Please share your complete number."
      },
      PhoneNumber: {
        minlength: "Please share your complete number."
      },
      PortfolioLink: {
        required: "Please link your portfolio — this is important!"
      },
      Message: {
        required: "Don't forget to answer this!"
      },
      ResumeCV: {
        required: "Please upload your resumé."
      },
    },
    submitHandler : function(form) {
      form.submit();
    }
  });
}

validateCreativesForm();

/** -- Careers Page - Operations Form --**/

charactersOnly('NameField');

let validateOperationsForm = () => {
  $.validator.addMethod("lettersOnly", function(value, element) {
    return this.optional(element) || /^[A-Za-zÀ-Üà-ü'.-\s]+$/i.test(value);
  }, "Please use regular letters only.");
  
  $.validator.addMethod("emailordomain", function(value, element) {
    return this.optional(element) || /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/.test(value) || /[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/.test(value);
  }, "Please enter a valid email address.");

  $.validator.addMethod("mobilePattern", function(value, element) {
      return this.optional(element) || /^[+\d](?:.*\d)?$/.test(value);
  }, "Please use numbers only.");

  $('#operations-form').validate({
    onsubmit: false,
    errorPlacement: function(error, element) {
      $(element)
        .closest("form")
        .find("label[for='" + element.attr("id") + "']" )
        .closest('.career-field').addClass('hasError')
        .append(error);
    },
    rules: {
      PositionField: {
          required: true,
      },
      NameField: {
        required: true,
        lettersOnly: true,
      },
      EmailAdd: {
        required: true,
        emailordomain: true
      },
      Mobile: {
          required: true,
          mobilePattern: true,
          minlength: 7
      },
      Phone: {
          mobilePattern: true,
          minlength: 7
      },
      MessageField: {
          required: true,
      },
      ResumeorCV: {
        required: true
      }
    },
    errorElement: "span",
    messages: {
      PositionField: {
         required : "What position are you applying for?"
      },
      NameField: {
        required: "Please tell us your name!"
      },
      EmailAdd: {
        required: "Don't forget your email!"
      },
      Mobile: {
        required: "Please share your complete number.",
        minlength: "Please share your complete number."
      },
      Phone: {
        minlength: "Please share your complete number."
      },
      MessageField: {
        required: "Don't forget to answer this!"
      },
      ResumeorCV: {
        required: "Please upload your resumé."
      },
    },
    submitHandler : function(form) {
      form.submit();
    }
  });
}

validateOperationsForm();

/*--------------------- 
/  Career Entry Page
----------------------*/

/** -- Career Entry Page - Creatives Form --**/

let validateCareerEntryForm__Creatives = () => {
  $.validator.addMethod("lettersOnly", function(value, element) {
    return this.optional(element) || /^[A-Za-zÀ-Üà-ü'.-\s]+$/i.test(value);
  }, "Please use regular letters only.");
  
  $.validator.addMethod("emailordomain", function(value, element) {
    return this.optional(element) || /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/.test(value) || /[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/.test(value);
  }, "Please enter a valid email address.");

  $.validator.addMethod("mobilePattern", function(value, element) {
      return this.optional(element) || /^[+\d](?:.*\d)?$/.test(value);
      }, "Please use numbers only.");

  $.validator.addMethod("validPortfolio", function(value, element) {
    return this.optional(element) || /(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]+\.[^\s]{2,}|www\.[a-zA-Z0-9]+\.[^\s]{2,})/gi.test(value);
  }, "Invalid portfolio link!");

  $('#careers-application').validate({
    onsubmit: false,
    errorPlacement: function(error, element) {
      $(element)
        .closest("form")
        .find("label[for='" + element.attr("id") + "']" )
        .closest('.career-entry-field').addClass('hasError')
        .append(error);
    },
    rules: {
      Name: {
        required: true,
        lettersOnly: true,
      },
      EmailAddress: {
        required: true,
        emailordomain: true
      },
      MobileNumber: {
          required: true,
          mobilePattern: true,
          minlength: 7
      },
      PhoneNumber: {
          mobilePattern: true,
          minlength: 7
      },
      PortfolioLink: {
          required: true,
          validPortfolio: true
      },
      Message: {
          required: true,
      },
      ResumeCV: {
        required: true
      }
    },
    errorElement: "span",
    messages: {
      Name: {
        required: "Please tell us your name!"
      },
      EmailAddress: {
        required: "Don't forget your email!"
      },
      MobileNumber: {
        required: "Please share your complete number.",
        minlength: "Please share your complete number."
      },
      PhoneNumber: {
        minlength: "Please share your complete number."
      },
      PortfolioLink: {
        required: "Please link your portfolio — this is important!"
      },
      Message: {
        required: "Don't forget to answer this!"
      },
      ResumeCV: {
        required: "Please upload your resumé."
      },
    },
    submitHandler : function(form) {
      form.submit();
    }
  });
}

validateCareerEntryForm__Creatives();

/** -- Career Entry Page - Operations Form --**/

let validateCareerEntryForm__Operations = () => {
  $.validator.addMethod("lettersOnly", function(value, element) {
    return this.optional(element) || /^[A-Za-zÀ-Üà-ü'.-\s]+$/i.test(value);
  }, "Please use regular letters only.");
  
  $.validator.addMethod("emailordomain", function(value, element) {
    return this.optional(element) || /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/.test(value) || /[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/.test(value);
  }, "Please enter a valid email address.");

  $.validator.addMethod("mobilePattern", function(value, element) {
      return this.optional(element) || /^[+\d](?:.*\d)?$/.test(value);
  }, "Please use numbers only.");

  $('#careers-application-operations').validate({
    onsubmit: false,
    errorPlacement: function(error, element) {
      $(element)
        .closest("form")
        .find("label[for='" + element.attr("id") + "']" )
        .closest('.career-entry-field').addClass('hasError')
        .append(error);
    },
    rules: {
      Name: {
        required: true,
        lettersOnly: true,
      },
      EmailAddress: {
        required: true,
        emailordomain: true
      },
      MobileNumber: {
        required: true,
        mobilePattern: true,
        minlength: 7
      },
      PhoneNumber: {
        mobilePattern: true,
        minlength: 7
      },
      Message: {
          required: true,
      },
      ResumeCV: {
        required: true
      }
    },
    errorElement: "span",
    messages: {
      Name: {
        required: "Please tell us your name!"
      },
      EmailAddress: {
        required: "Don't forget your email!"
      },
      MobileNumber: {
        required: "Please share your complete number.",
        minlength: "Please share your complete number."
      },
      PhoneNumber: {
        minlength: "Please share your complete number."
      },
      Message: {
        required: "Don't forget to answer this!"
      },
      ResumeCV: {
        required: "Please upload your resumé."
      },
    },
    submitHandler : function(form) {
      form.submit();
    }
  });
}

validateCareerEntryForm__Operations();

/*--------------------- 
/  Contact Us Page
----------------------*/

let validateContactUsForm = () => {

  $.validator.addMethod("emailordomain", function(value, element) {
    return this.optional(element) || /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/.test(value) || /[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/.test(value);
  }, "Please enter a valid email address.");

  $.validator.addMethod("lettersOnly", function(value, element) {
    return this.optional(element) || /^[A-Za-zÀ-Üà-ü'.-\s]+$/i.test(value);
  }, "Please use regular letters only.");

  $('#wf-form-Email-Form').validate({
    onsubmit: false,
    errorPlacement: function(error, element) {
      $(element)
        .closest("form")
        .find("label[for='" + element.attr("id") + "']" )
        .closest('.contact-field').addClass('hasError')
        .append(error);
    },
    rules: {
      Name: {
        required: true,
        lettersOnly: true,
      },
      Email: {
        required: true,
        emailordomain: true
      },
      Message: {
        required: true
      }
    },
    errorElement: "span",
    messages: {
      Name: {
        required: "Please tell us your name!"
      },
      Email: {
        required: "Don't forget your email!"
      },
      Message: {
        required: "Don't forget to answer this!"
      }
    },
    submitHandler : function(form) {
      form.submit();
    }
  });
}

validateContactUsForm();

let submitForm = (formId) => {
  
  $(formId).submit(function (e) {

    if ($(formId).valid()){
      return true;
    } else {
      e.preventDefault();
      return false;
    }
  
  });
}

let submitModalForm = (formId) => {
  $(formId).submit(function (e) {

    if ($(formId).valid()){
      $('.application-form-error').removeClass('visible');
      $('.tab-menu, .tab-selection-label').addClass('hide');
      return true;
    } else {
      e.preventDefault();
      return false;
    }

  });
}

submitForm('#wf-form-Email-Form');

submitForm('#careers-application');
submitForm('#careers-application-operations');

submitModalForm('#creatives-form');
submitModalForm('#operations-form');


let addClassToUL = () => {
  $('.career-entry-description > ul').prev('h3').addClass('career-description-title');
  $('.career-entry-description > h5').addClass('career-description-title sub-heading');
  $('h5').prev('h3').addClass('career-description-title no-list');
  $('h3').next('ul').addClass('career-entry-list');
  $('h5').next('ul').addClass('career-entry-sub-heading-list');

  $('<div class="separator"></div>').insertBefore(".career-description-title");
  $('.career-description-title.sub-heading').prev('div').remove();
  $('.career-entry-description > .separator:first-child').remove();
}

addClassToUL();


////////////////////////////////////////////////////////////////////////
// Slider Animation

let sliderContainer = $('.slider-panels-3d-plane');

let setupSlider = ($slider) => {
  let slides            = $("._3d-pane", $slider);
  let $root             = $slider.parent().parent();
  let $markersContainer = $root.find('.slider-marker');
  let $tabsContainer    = $root.find('.expertise-selection');

  let $leftHitArea  = $('> .slider-large-hit-area--left', $root);
  let $rightHitArea = $('> .slider-large-hit-area--right', $root);

  // Set the width of the slider container
  let $slidesContainer = $root.find('> .slider-panel-plane > .slider-panels-3d-plane');
  // Resize to the number of available elements
  $slidesContainer.width(slides.length * 100 + `vw`);

  ////////////////////////////////////////////////////////////////
  // Markers
  // Clean the marker first
  $markersContainer.find('.slider-mark').remove();

  // Inject the markers
  for (let a = 0; a < slides.length; a++) {
    $markersContainer.append(`<li data-slide="${a + 1}" class="slider-mark"></li>`);
  }

  $root.position = 0;

  let sliderObject = {
    root     : $root,
    container: $slidesContainer,
    markers  : $markersContainer,
    tabs     : $tabsContainer
  }

  // Bind Click on the marker
  $markersContainer.find('.slider-mark').on('click', (e) => {
    let $el = $(e.currentTarget);
    let position = $el.attr('data-slide');

    // Hide Left Hit Area when hitting first element
    if (position == 1) {
      $leftHitArea.hide();
      $rightHitArea.show();
    }
    // Hide Right Hit Area when hitting last element
    if (position == slides.length) { 
      $rightHitArea.hide();
      $leftHitArea.show();
    }

    // Show both when position is > 1 & < slides.length
    if ((position > 1) && (position < slides.length)) {
      $rightHitArea.show();
      $leftHitArea.show();
    } 

    goToSlide(sliderObject, position);
  });

  // Bind Click on the tab item
  $tabsContainer.find('.expertise-selection-item').on('click', (e) => {
    let $el = $(e.currentTarget);
    let position = $el.attr('data-slide');

    // Hide Left Hit Area when hitting first element
    if (position == 1) {
      $leftHitArea.hide();
      $rightHitArea.show();
    }
    // Hide Right Hit Area when hitting last element
    if (position == slides.length) { 
      $rightHitArea.hide();
      $leftHitArea.show();
    }

    // Show both when position is > 1 & < slides.length
    if ((position > 1) && (position < slides.length)) {
      $rightHitArea.show();
      $leftHitArea.show();
    } 

    goToSlide(sliderObject, position);
  });

  // Hide Left Hit Area on Page Load
  $leftHitArea.hide();

  $leftHitArea.on('click', (e) => {
    $rightHitArea.show();

    // Reached the first element
    if (sliderObject.root.position == 1) { 
      return; 
    } 

    // Hide Left Hit Area when hitting first element
    if (sliderObject.root.position == 2) {
      $leftHitArea.hide();
      $rightHitArea.show();
    }

    goToSlide(sliderObject, sliderObject.root.position - 1);
  });

  $rightHitArea.on('click', (e) => {
    //Show Left Hit Area on Page Load
    $leftHitArea.show();

    // Reached the last element
    if (sliderObject.root.position == slides.length) { 
      return; 
    }

    // Hide Right Hit Area when hitting last element
    if (sliderObject.root.position == slides.length-1) { 
      $rightHitArea.hide();
    }

    goToSlide(sliderObject, sliderObject.root.position + 1);
  });

  goToSlide(sliderObject, 1, $leftHitArea, $rightHitArea);
};

let goToSlide = (sliderObject, n) => {

  n = parseInt(n);

  let x            = (n - 1) * -100;
  let $markers     = $('.slider-mark', sliderObject.markers);
  let $sliderTitle = $('.slider-title-block', sliderObject.root);

  let $tabs = $('.expertise-selection-item > .tab-item', sliderObject.tabs);
  
  $('.slider-title', $sliderTitle).css({
    'margin-right': 0,
    'margin-left' : 0
  });


  // Set the current position
  sliderObject.root.position = n;

  // Move to position
  anime({
    targets   : sliderObject.container[0],
    keyframes: [
      {
        translateZ: -220,
        opacity   : .6,
        duration  : 400,
        easing    : 'easeInOutExpo'
      },
      {
        delay     : 50,
        translateX: `${x}vw`,
        duration  : 250,
        easing    : 'cubicBezier(0.570, 0.000, 0.000, 1.000);'
      },
      {
        translateZ: 0,
        duration  : 350,
        opacity   : 1,
        easing    : 'easeInOutExpo',
        delay     : 100
      }
    ]
  });


  // move the title cards as well
  // add some bit of delay
  anime({
    targets: $sliderTitle[0],
    keyframes: [
      {
        translateZ: 250,
        duration: 350,
        easing: 'easeInOutExpo',
        opacity: .8,
        delay: 50
      },
      {
        delay     : 320,
        translateX: `${x}vw`,
        duration  : 195,
        easing    : 'cubicBezier(0.570, 0.000, 0.000, 1.000);'
      },
      {
        translateZ: 0,
        opacity: 1,
        duration: 300,
        easing: 'easeInOutExpo',
        delay: 100
      },
    ]
  });

  let titles = [];
  $('.slider-title', $sliderTitle).each((i, el) => {
    titles.push(el);
  });

  let translateXvalPos;
  let translateXvalNega;
  
  let screenWidth = window.innerWidth;

  if (screenWidth > 491) {
    translateXvalPos = '43vw';
    translateXvalNega = '-43vw';
  } else {
    translateXvalPos = '0';
    translateXvalNega = '0';
  }

  anime({
    targets    : titles,
    translateX : 0,
    easing     : 'easeInOutExpo',
    duration   : 400,
    delay      : 200
  });

  anime({
    targets    : titles[n],
    translateX : translateXvalNega,
    easing     : 'easeInOutExpo',
    duration   : 550,
    delay      : 800
  });

  anime({
    targets    : titles[n - 2],
    translateX: translateXvalPos,
    easing     : 'easeInOutExpo',
    duration   : 550,
    delay      : 800
  });

  // Mark the marker as active
  $markers.removeClass('active');
  $($markers[n - 1]).addClass('active');

  // Mark the marker as active
  $tabs.removeClass('active');
  $($tabs[n - 1]).addClass('active');
};

sliderContainer.each((i, slider) => {
  let $slider = $(slider);
  setupSlider($slider); 
});